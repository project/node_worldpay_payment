<?php
/**
 * @file
 * Contains \Drupal\node_worldpay_payment\Form\WorldPaySettingsForm.
 */
namespace Drupal\node_worldpay_payment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class WorldPaySettingsForm extends ConfigFormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'worldpay_config_admin_settings';
  }

  /**
   * @inheritdoc
   */
  protected function getEditableConfigNames() {
    return [
      'node_worldpay_payment.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_worldpay_payment.settings');
    
    $form['worldpay_installation_id'] = [
      '#type' => 'textfield',
      '#title' => t('Installation ID'),
      '#size' => 16,
      '#default_value' => $config->get('worldpay_installation_id'),
      '#required' => TRUE,
    ];
    
    $form['worldpay_payment_mode'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable test mode'),
      '#default_value' => $config->get('worldpay_payment_mode'),
    ];
    
    $form['payment_urls_test'] = [
      '#type' => 'textfield',
      '#title' => t('Test URL'),
      '#description' => t('The WorldPay test environment URL.'),
      '#default_value' => $config->get('worldpay_test_url'),
      '#required' => TRUE,
    ];

    $form['payment_urls_live'] = [
      '#type' => 'textfield',
      '#title' => t('Live URL'),
      '#description' => t('The WorldPay live environment URL.'),
      '#default_value' => $config->get('worldpay_live_url'),
      '#required' => TRUE,
    ];
    
    $form['worldpay_amount'] = [
      '#type' => 'textfield',
      '#title' => t('Amount'),
      '#size' => 16,
      '#default_value' => $config->get('worldpay_amount'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('node_worldpay_payment.settings')
      ->set('worldpay_installation_id', $form_state->getValue('worldpay_installation_id'))
      ->set('worldpay_payment_mode', $form_state->getValue('worldpay_payment_mode'))
      ->set('worldpay_test_url', $form_state->getValue('payment_urls_test'))
      ->set('worldpay_live_url', $form_state->getValue('payment_urls_live'))
      ->set('worldpay_amount', $form_state->getValue('worldpay_amount'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}