<?php

namespace Drupal\node_worldpay_payment\Form;

/**
 * @file
 * Contains \Drupal\node_worldpay_payment\Form\WorldpayPaymentForm.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Html;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;

/**
 * Contribute form.
 */
class WorldpayPaymentForm extends FormBase {

  protected $nid;

  /**
   * Constructs a new WorldpayPaymentForm instance.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->nid = $route_match->getParameter('nid');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'worldpay_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $_nid = $this->nid;

    $host = \Drupal::request()->getSchemeAndHttpHost();

    $callback_url = Url::fromRoute('node_worldpay_payment.worldpay_callback', [], ['absolute' => TRUE]);
    $callback_path = $callback_url->toString();

    $config = \Drupal::config('system.site');

    $worldpay_config = \Drupal::config('node_worldpay_payment.settings');
    $installation_id = $worldpay_config->get('worldpay_installation_id');
    $worldpay_amount = $worldpay_config->get('worldpay_amount');
    $worldpay_payment_mode = $worldpay_config->get('worldpay_payment_mode');
    $payment_urls_test = $worldpay_config->get('worldpay_test_url');
    $payment_urls_live = $worldpay_config->get('worldpay_live_url');

    if($worldpay_payment_mode){
      $form['#action'] = $payment_urls_test;
    }else{
      $form['#action'] = $payment_urls_live;
    }

    $user = \Drupal::currentUser();
    $user_email = $user->getEmail();

    $node = Node::load($_nid);
    $title = $node->getTitle();

    $form['siteTitle'] = array('#markup' => $config->get('name'));
    $form['instId'] = array('#markup' => $worldpay_config->get('worldpay_installation_id'));
    $form['amount'] = array('#markup' => $worldpay_amount);
    $form['currency'] = array('#markup' => 'GBP');
    $form['node_title'] = array('#markup' => $title);
    $form['node_orderId'] = array('#markup' => $_nid);
    $form['user_email'] = array('#markup' => $user_email);

    $form['worldpay_callback'] = array('#markup' => $callback_path);

    $form['worldpay_host'] = array('#markup' => $host);

    $form['worldpay_returnurl'] = array('#markup' => $callback_path );

    $form['node_id'] = array('#markup' => $_nid);

    $form['#theme'] = 'worldpay_payment_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}

?>
