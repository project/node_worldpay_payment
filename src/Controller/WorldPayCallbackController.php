<?php

namespace Drupal\node_worldpay_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;


/**
 * WorldPayCallbackController class.
 */
class WorldPayCallbackController extends ControllerBase {

  /**
   * WorldPay Callback.
   */
  public function worldPayCallback() {

    $nodeid = \Drupal::request()->request->get('MC_nodeid');
    $transId = \Drupal::request()->request->get('transId');
    $transStatus = \Drupal::request()->request->get('transStatus');

    \Drupal::logger('node_worldpay_payment')->notice("<pre>" . print_r($_POST, TRUE) . "</pre>");

    return $response;
  }

}
